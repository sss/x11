# Copyright 2008-2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2008, 2010 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2013-2017 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require cmake [ api=2 ] utf8-locale

export_exlib_phases src_configure src_test

SUMMARY="A PDF rendering library, based on Xpdf"
DESCRIPTION="
Poppler is a PDF rendering library derived from xpdf. It has been enhanced to
utilize modern libraries such as freetype and cairo for better rendering. It
also provides basic command line utilities.
"
HOMEPAGE="https://${PN}.freedesktop.org"

# NOTE: test data tarball:
#   git://anongit.freedesktop.org/git/poppler/test
#   git archive --format=tar --prefix=test/ HEAD | xz > ${PN}-test-yyyymmdd.tar.xz
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.xz
           https://dev.exherbo.org/distfiles/${PN}/${PN}-test-20090513.tar.xz"

REMOTE_IDS="freecode:${PN}"

LICENCES="|| ( GPL-2 GPL-3 )"
SLOT="0"
MYOPTIONS="
    cairo     [[ description = [ Build the cairo graphics backend ] ]]
    curl      [[ description = [ Use libcurl for HTTP support ] ]]
    glib      [[
        description = [ Build the GLib wrapper ]
        requires = cairo
    ]]
    gobject-introspection [[ requires = glib ]]
    jpeg2000  [[ description = [ Enable support for the JPEG2000 codec ] ]]
    lcms      [[ description = [ Use Little CMS for color management ] ]]
    qt5       [[ description = [ Build the Qt5 wrapper ] ]]
    tiff

    ( cairo qt5 ) [[ number-selected = at-least-one ]]
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

if ! ever at_least 0.62.0 ; then
    MYOPTIONS+="
        qt4 [[ description = [ Build the Qt4 wrapper ] ]]
        ( cairo qt4 qt5 ) [[ number-selected = at-least-one ]]
    "
fi

DEPENDENCIES="
    build:
        virtual/pkg-config[>=0.9.0]
        gobject-introspection? ( gnome-desktop/gobject-introspection:1 )
        qt5? ( sys-devel/cmake[>=2.8.7] )
    build+run:
        app-text/poppler-data[>=0.4.7]
        dev-libs/nspr
        dev-libs/nss[>=3.19]
        media-libs/fontconfig[>=2.0.0]
        media-libs/freetype:2[>=2.1.8]
        media-libs/libpng:=
        sys-libs/zlib
        cairo? ( x11-libs/cairo[>=1.10.0] )
        curl? ( net-misc/curl )
        glib? ( dev-libs/glib:2[>=2.41] )
        jpeg2000? ( media-libs/OpenJPEG:2 )
        lcms? ( media-libs/lcms2 )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        qt5? ( x11-libs/qtbase:5 )
        tiff? ( media-libs/tiff )
"

if ! ever at_least 0.62.0 ; then
    DEPENDENCIES+="
        build+run:
            qt4? ( x11-libs/qt:4[>=4.7.0] )
    "
fi

poppler_src_configure() {
    local cmakeargs=(
        # considered experimental for now
        # https://bugs.freedesktop.org/show_bug.cgi?id=90795
        -DSPLASH_CMYK:BOOL=FALSE
        -DBUILD_CPP_TESTS:BOOL=$(expecting_tests TRUE FALSE)
        -DENABLE_CPP:BOOL=TRUE
        -DENABLE_DCTDECODER:STRING=libjpeg
        -DENABLE_LIBOPENJPEG:STRING=$(option jpeg2000 openjpeg2 none )
        -DENABLE_SPLASH:BOOL=TRUE
        -DENABLE_UTILS:BOOL=TRUE
        -DENABLE_XPDF_HEADERS:BOOL=TRUE
        -DENABLE_ZLIB:BOOL=TRUE
        -DENABLE_ZLIB_UNCOMPRESS:BOOL=FALSE
        -DWITH_JPEG:BOOL=TRUE
        -DWITH_NSS3:BOOL=TRUE
        -DWITH_PNG:BOOL=TRUE
        # GTK and it\'s tests seem to be unused as of poppler-0.22.1
        -DBUILD_GTK_TESTS:BOOL=FALSE
        -DWITH_GTK:BOOL=FALSE
        $(cmake_enable curl LIBCURL)
        $(cmake_with cairo Cairo)
        $(cmake_with glib GLIB)
        $(cmake_with gobject-introspection GObjectIntrospection)
        $(cmake_with tiff TIFF)
    )

    if option qt5 ; then
        cmakeargs+=(
            -DBUILD_QT5_TESTS:BOOL=$(expecting_tests TRUE FALSE)
        )
    fi

    if ever at_least 0.62.0 ; then
        cmakeargs+=(
            -DBUILD_SHARED_LIBS:BOOL=TRUE
            -DENABLE_CMS:STRING=$(option lcms lcms2 none )
            -DENABLE_GTK_DOC:BOOL=FALSE
            $(cmake_enable glib GLIB)
            $(cmake_enable qt5 QT5)
        )
    else
        cmakeargs+=(
            -DSHARE_INSTALL_DIR=/usr/share
            -DENABLE_CMS:STRING=$(option lcms lcms2 )
            $(cmake_disable_find qt5 Qt5Core)
            $(cmake_disable_find qt5 Qt5Gui)
            $(cmake_disable_find qt5 Qt5Test)
            $(cmake_disable_find qt5 Qt5Widgets)
            $(cmake_disable_find qt5 Qt5Xml)
            $(cmake_with qt4 Qt4)
        )
        if option qt4 ; then
            cmakeargs+=(
                -DBUILD_QT4_TESTS:BOOL=$(expecting_tests TRUE FALSE)
            )
        fi
    fi

    ecmake \
        "${cmakeargs[@]}"
}

poppler_src_test() {
    # qt4/tests/check_password.cpp fails with LC_ALL=C
    require_utf8_locale

    default
}

