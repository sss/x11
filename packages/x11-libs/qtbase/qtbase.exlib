# Copyright 2008-2010 Bo Ørsted Andresen <zlin@exherbo.org>
# Copyright 2008-2009, 2010 Ingmar Vanhassel
# Copyright 2013-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'qt-4.3.4-r1.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require qt
require option-renames [ renames=[ 'systemd journald' ] ]

export_exlib_phases src_prepare src_configure src_compile src_install

SUMMARY="Qt Cross-platform application framework for desktop and embedded development"
DESCRIPTION="Contains the following modules:
- QtCore: non-graphical classes used by other modules
- QtConcurrent: Classes for writing multi-threaded programs w/o using low-level threading primitives
- QtDBus: Classes for inter-process communication over the D-Bus protocol
- QtGui: Base classes for graphical user interface (GUI) components
- QtNetwork: Classes to make network programming easier and more portable
- QtOpenGL: OpenGL support classes
- QtPrintSupport: Classes to make printing easier and more portable
- QtSql: Classes for database integration using SQL
- QtTest: Classes for unit testing Qt applications and libraries
- QtWidgets: Classes to extend Qt GUI with C++ widgets (belonged to QtGui in 4.x)
- QtXml: C++ implementations of SAX and DOM"

MYOPTIONS_PARTS="doc examples"
SQL_BACKENDS="mysql postgresql sqlite" # firebird odbc
MYOPTIONS="cups debug glib gui journald kms opengles sql
    ${MYOPTIONS_PARTS}
    ( ${SQL_BACKENDS} ) [[ requires = sql ]]
    sql? (
        ( ${SQL_BACKENDS} ) [[ number-selected = at-least-one ]]
    )
    cups        [[ requires    = gui ]]
    examples    [[ requires    = gui ]]
    glib        [[ description = [ Add support for the glib eventloop ] ]]
    gtk         [[ description = [ Enable GTK+ style support, this will install a Qt4 style that renders using GTK+, to blend in with a GTK+ desktop ]
                   requires    = [ glib gui ] ]]
    gui         [[ description = [ Build GUI related modules ]
                   presumed    = true ]]
    journald    [[ description = [ Support sending logging output directly to systemd-journald ] ]]
    kms         [[ description = [ KMS platform plugin to run a single-window per screen without X11 ]
                   requires    = opengles ]]
    libinput    [[ description = [ Support input devices via libinput ]
                   requires = gui ]]
    libproxy    [[ description = [ Use libproxy for system proxy settings ] ]]
    opengles    [[ description = [ Use OpenGL ES2 rather than standard desktop OpenGL ]
                   requires    = gui ]]
    sql         [[ description = [ Build the QtSQL module ] ]]

    ( platform: amd64 x86 )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
    ( providers: libressl openssl ) [[ number-selected = exactly-one ]]
"

if ever at_least 5.10.0-beta4 ; then
    MYOPTIONS+="
        vulkan [[ description = [ Support for rendering via the Vulkan graphics API ]
                  requires = gui ]]
    "
fi

MYOPTIONS+="
    amd64_cpu_features: sse3 ssse3 sse4.1 sse4.2 avx avx2 avx512 f16c
    arm_cpu_features: neon
    x86_cpu_features: sse2 sse3 ssse3 sse4.1 sse4.2 avx avx2 avx512 f16c
"

DEPENDENCIES="
    build:
        virtual/pkg-config
        doc? (
            x11-libs/qttools:${SLOT}[>=5.8.0-rc] [[
                note = [ qtattributionsscanner (5.8.0-rc), qhelpgenerator ]
            ]]
        )
    build+run:
        app-admin/eclectic[>=2.0.18] [[ note = [ Split ld-*.path, @TARGET@ substitution ] ]]
        dev-libs/double-conversion
        dev-libs/icu:=
        dev-libs/pcre2[>=10.20]
        sys-apps/dbus[>=1.2.0] [[ note = [ besides QtDBUs also needed for accessibility and xcb ] ]]
        sys-libs/zlib[>=1.0.8]
        cups? ( net-print/cups[>=1.4] )
        glib? ( dev-libs/glib:2 )
        gtk? (
            dev-libs/atk
            x11-libs/gtk+:3[>=3.6]
            x11-libs/pango
        )
        gui? (
            dev-libs/at-spi2-core
            media-libs/fontconfig
            media-libs/freetype:2[>=2.2]
            media-libs/libpng:=
            x11-dri/mesa
            x11-libs/harfbuzz[>=0.9.42]
            x11-libs/libICE
            x11-libs/libSM
            x11-libs/libX11
            x11-libs/libXext
            x11-libs/libXi
            x11-libs/libxcb[>=1.10]
            x11-libs/libxkbcommon[>=0.4.1][X]
            x11-libs/mtdev
            x11-utils/xcb-util-image
            x11-utils/xcb-util-keysyms
            x11-utils/xcb-util-renderutil
            x11-utils/xcb-util-wm
            providers:ijg-jpeg? ( media-libs/jpeg:= )
            providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        )
        journald? ( sys-apps/systemd ) [[ note = [ wanted for journald logging ] ]]
        kms? ( x11-dri/libdrm )
        libinput? ( sys-libs/libinput )
        libproxy? ( net-libs/libproxy )
        mysql? ( virtual/mysql )
        postgresql? ( dev-db/postgresql )
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
        sqlite? ( dev-db/sqlite:3 )
"

if ever at_least 5.10.0-beta4 ; then
    DEPENDENCIES+="
        build+run:
            gui? ( x11-libs/libXrender[>=0.6] )
            vulkan? ( sys-libs/vulkan )
    "
fi

# Packages that need QtSQL should depend on x11-libs/qtbase:5[sql]
# option='sql' needs at least one SQL plugin, any-of mysql, postgresql, sqlite, (firebird, odbc)

# Tests need a running X server and network access (last checked: 5.1.1)
RESTRICT="test"

qconf5() {
    local myconf=() host=$(exhost --target)

    # paths
    myconf+=(
        -platform $(qt_mkspecs_dir)
        -prefix /usr/${host}
        -bindir /usr/${host}/lib/qt5/bin
        -docdir /usr/share/doc/qt-${PVR}
        -headerdir /usr/${host}/include/qt5
        -libdir /usr/${host}/lib
        -archdatadir /usr/${host}/lib/qt5
        -plugindir /usr/${host}/lib/qt5/plugins
        -libexecdir /usr/${host}/lib/qt5/libexec
        -datadir /usr/share/qt5
        -qmldir /usr/${host}/lib/qt5/qml
        -translationdir /usr/share/qt5/translations
        -sysconfdir /etc/qt5
        -examplesdir /usr/share/qt5/examples
        -importdir /usr/${host}/lib/qt5/imports
        -testsdir /usr/share/qt5/tests
    )

    edo "${ECONF_SOURCE:-.}"/configure "${myconf[@]}" "$@"
}

qtbase_src_prepare() {
    default

    # Don't prestrip.
    edo sed -e "/^CONFIG +=/s:$: nostrip:" -i qmake/qmake.pro

    # With -gcc and -g++ Qt fails to recognize gcc's version, which leads to
    # build failures with qtwebengine. It's safe to use -gcc and -g++ instead
    # of -cc -and -cxx here, because we only modifiy gcc mkspecs files.
    edo sed -e "s:gcc:$(exhost --target)-gcc:" \
            -e "s:g++:$(exhost --target)-g++:" \
            -i "${WORK}"/mkspecs/common/g++-base.conf

    edo sed -e "s:QMAKE_CFLAGS_RELEASE.*=.*:QMAKE_CFLAGS_RELEASE=${CPPFLAGS} ${CFLAGS} ${ASFLAGS}:" \
            -e "s:QMAKE_CXXFLAGS_RELEASE.*=.*:QMAKE_CXXFLAGS_RELEASE=${CPPFLAGS} ${CXXFLAGS} ${ASFLAGS}:" \
            -i "${WORK}"/mkspecs/common/gcc-base.conf

    edo sed -e "s:QMAKE_LFLAGS_RELEASE.*=.*:QMAKE_LFLAGS_RELEASE=${LDFLAGS}:" \
            -i "${WORK}"/mkspecs/common/g++-unix.conf

    # Workaround to allow other projects to find Qt's private headers via CMake
    # https://bugreports.qt-project.org/browse/QTBUG-37417
    edo sed -e "/CMAKE_NO_PRIVATE_INCLUDES = true/d" \
            -i mkspecs/features/create_cmake.prf

    # Fix the names of some unprefixed executables
    edo sed -e "s:\(PKG_CONFIG.*=.\)\(pkg-config\):\1${PKG_CONFIG}:" \
            -i mkspecs/features/qt_functions.prf

    edo sed -e "s:^\(QMAKE_AR.*=.\)\(ar.*\):\1$(exhost --tool-prefix)\2:" \
            -i mkspecs/common/linux.conf

    # Fix a non-fatal "sh: readelf: command not found" during src_compile
    edo sed -e "s:\(readelf\).*-l.*/bin/ls:$(exhost --tool-prefix)\1 -l /usr/$(exhost --target)/bin/ls:" \
            -i src/corelib/global/global.pri

    # Work around https://bugreports.qt.io/browse/QTBUG-47240 which prevents
    # knewstuff to build successfully if attica:0 and attica:5 are installed
    # at the same time.
    edo sed -e "/\!isEmpty(QMAKE_INCDIR_OPENGL)/d" \
            -i src/gui/gui.pro
}

qtbase_src_configure() {
    local myconf=(
        -confirm-license
        -opensource
        -release
        -verbose

        -accessibility
        -dbus-linked
        $(option debug && echo "-debug")
        -icu
        -no-ccache
        -no-eglfs    # EGL Full Screen/Single Surface, probably only useful on embedded devices
        -no-directfb
        -no-imf
        -no-lgmon      # Both are only useful on QNX
        -no-linuxfb
        -no-mirclient  # No need for stuff which is only used by Canonical
        -no-optimize-size
        -no-rpath
        -no-separate-debug-info
        -no-tslib   # Touchscreen access library - unwritten
        -no-use-gold-linker   # let eclectic ld choose this
        -openssl-linked
        -pch
        -pkg-config
        $(if [[ $(exhost --target) =~ (i686-*|x86_64-*) ]] ; then
            echo '-reduce-relocations'
          fi)
        -syslog
        -system-doubleconversion
        -system-pcre
        -system-proxies
        -system-xcb
        -system-xkbcommon-x11
        -system-zlib
        $(qt_enable cups)
        $(qt_enable glib)
        $(qt_enable gtk)
        $(qt_enable gui)
        $(option gui && echo '-system-freetype')
        $(option gui && echo '-system-harfbuzz' || echo -no-harfbuzz)
        $(option gui && echo '-system-libjpeg')
        $(option gui && echo '-system-libpng')
        $(qt_enable journald)
        $(qt_enable kms)
        $(qt_enable libinput)
        $(qt_enable libproxy)
        $(ever at_least 5.10.0-beta4 && qt_enable vulkan)
    )

    # Avoid auto detection of CPU features:
    # sse2, sse3, ssse3, sse4.1, sse4.2, avx, avx2, avx512 neon
    # Always enable sse2 on amd64
    # FIXME: needs to be handled on cross (heirecka)
    if option !platform:amd64 && option !x86_cpu_features:sse2 ; then
        myconf+=( -no-sse2 )
    fi
    for feature in sse3 ssse3 sse4.1 sse4.2 avx avx2 avx512 ; do
        if option !amd64_cpu_features:${feature} && option !x86_cpu_features:${feature} ; then
            myconf+=( -no-${feature} )
        fi
    done

    # X
    local o X_options="fontconfig sm xkb"
    for o in ${X_options}; do
        myconf+=( $(qt_enable gui ${o}) )
    done

    # Other GUI options
    local g GUI_options="egl evdev gbm mtdev widgets xcb xcb-xlib xinput2 xkbcommon-evdev"
    for g in ${GUI_options}; do
        myconf+=( $(qt_enable gui ${g}) )
    done

    if option gui ; then
        myconf+=( $(option opengles && echo -opengl es2 || echo -opengl desktop) )
    else
        myconf+=( -no-opengl )
    fi

    myconf+=(
        -no-sql-db2       # IBM DB2 (version 7.1 and above)
        -no-sql-ibase     # Borland InterBase
        -no-sql-oci       # Open Database Connectivity (ODBC) (e.g. MS SQL Server)
        -no-sql-odbc      # Oracle Call Interface Driver
        -no-sql-sqlite2   # SQLite version 2
        -no-sql-tds       # Sybase Adaptive Server
        $(qt_enable mysql sql-mysql plugin "")
        $(option mysql      && echo "-I/usr/$(exhost --target)/include/mysql -L/usr/$(exhost --target)/lib/mysql/")
        $(qt_enable postgresql sql-psql plugin "")
        $(option postgresql && echo "-I/usr/$(exhost --target)/include/postgresql/server/")
        $(qt_enable sqlite sql-sqlite plugin "" -system-sqlite)
    )

    # optional parts
    myconf+=(
        -make libs
        -make tools
        $(qt_build examples)
        $(expecting_tests && echo "-make tests" || echo "-nomake tests")
    )

    # Set {C,CXX,LD}FLAGS.
    # Do not link with -rpath (Gentoo bug #75181).
    edo sed -e "s:QMAKE_CFLAGS_RELEASE.*=.*:QMAKE_CFLAGS_RELEASE=${CFLAGS}:" \
            -e "s:QMAKE_CXXFLAGS_RELEASE.*=.*:QMAKE_CXXFLAGS_RELEASE=${CXXFLAGS}:" \
            -e "s:QMAKE_LFLAGS_RELEASE.*=.*:QMAKE_LFLAGS_RELEASE=${LDFLAGS}:" \
            -e "/CONFIG/s:$: nostrip:" \
            -e "s:QMAKE_RPATH.*=.*:QMAKE_RPATH=:" \
            -e "s:X11R6/::" \
            -i "${WORK}"/mkspecs/$(qt_mkspecs_dir)/qmake.conf

    # use gcc for linking
        unset LD

    qconf5 "${myconf[@]}"

    edo sed -e "s:^CXXFLAGS.*=:CXXFLAGS = ${CXXFLAGS} $(EXTRA_CXXFLAGS) $(CONFIG_CXXFLAGS) $(CPPFLAGS):" \
            -e "s:^LFLAGS.*=:LFLAGS = ${LDFLAGS} $(EXTRA_LFLAGS) $(CONFIG_LFLAGS):" \
            -i "${WORK}"/qmake/Makefile
}

qtbase_src_compile() {
    default

    if option doc; then
        # qt expects qhelpgenerator from qttools in workdir/bin, so symlink it to there
        edo ln -s /usr/$(exhost --target)/lib/qt5/bin/qhelpgenerator \
            "${WORK}"/bin/qhelpgenerator
        # html_docs target builds html docs, qch_docs QCH files (assistant), docs both
        option doc && emake docs
    fi
}

qtbase_src_install() {
    default

    local host=$(exhost --target)

    # remove build dir from libraries
    edo sed -i -e "s:${WORK}/lib:/usr/${host}/lib/qt5:g" "${IMAGE}"/usr/${host}/lib/{*.la,*.prl,pkgconfig/*.pc}

    hereenvd 44qt5 <<EOF
LDPATH=/usr/@TARGET@/lib/qt5
QT_PLUGIN_PATH=/usr/host/lib/qt5/plugins
EOF

    if option examples ; then
        # The examples might add some empty dirs. Since the examples are additional documentation,
        # there shouldn't be any empty dirs we might want to keep, so we remove them all in
        # one go.
        edo find "${IMAGE}"/usr/share/qt5/examples -type d -empty -delete
    fi

    # install documentation
    if option doc ; then
        local modules
        modules=( qtcore qtconcurrent qtdbus qtnetwork qttestlib qtxml )
        option gui && modules+=( qtgui qtopengl qtprintsupport qtwidgets )
        option sql && modules+=( qtsql )

        # QCH-Files e.g. for Qt Assistant...
        for i in ${modules[@]} ; do
            dodoc "${WORK}"/doc/${i}.qch
        done

        # ... and HTML documentation
        docinto html
        edo pushd "${WORK}"/doc
        for i in ${modules[@]} ; do
            dodoc -r ${i}
        done
        edo popd
    fi

    # remove references to build dir
    edo sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" "${IMAGE}"/usr/${host}/lib/libQt5*.prl

    # install symlinks for some binaries to /usr/bin
    local binaries
    edo mkdir "${IMAGE}"/usr/${host}/bin
    binaries=( moc qmake rcc $(option gui && echo "uic") )

    for i in ${binaries[@]} ; do
        [[ -e "${IMAGE}"/usr/${host}/lib/qt5/bin/${i} ]] || die "/usr/${host}/lib/qt5/bin/${i} does not exist in ${IMAGE}"
        dosym /usr/${host}/lib/qt5/bin/${i} /usr/${host}/bin/${i}-qt5
    done

    # disable debug output from applications by default
    insinto /etc/xdg/QtProject
    hereins qtlogging.ini <<EOF
[Rules]
*.debug=false
EOF
}

